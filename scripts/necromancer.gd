extends KinematicBody2D

class_name necromancer

export (int) var speed = 150

onready var sprite = $AnimatedSprite
onready var visivel = $VisibilityNotifier2D

var players = []

onready var minion = preload("res://scenes/enemy0.tscn")

var velocity = Vector2()

var sequency = [0,0,0,0,0]

var index = 0
var predict_index = 0

signal killed

func _ready():
	var sprites = [preload("res://assets/0x72_DungeonTilesetII_v1.3/frames/sym0_0.png"), preload("res://assets/0x72_DungeonTilesetII_v1.3/frames/sym1_0.png"), preload("res://assets/0x72_DungeonTilesetII_v1.3/frames/sym2_0.png"), preload("res://assets/0x72_DungeonTilesetII_v1.3/frames/sym3_0.png")]
	# gerando a sequencia de comandos a serem apertadas para derrotar o miniorc
	randomize()
	sequency[0] = int(rand_range(0,4))
	$HBoxContainer/s0.set_texture(sprites[sequency[0]])
	randomize()
	sequency[1] = int(rand_range(0,4))
	$HBoxContainer/s1.set_texture(sprites[sequency[1]])
	randomize()
	sequency[2] = int(rand_range(0,4))
	$HBoxContainer/s2.set_texture(sprites[sequency[2]])
	randomize()
	sequency[3] = int(rand_range(0,4))
	$HBoxContainer/s3.set_texture(sprites[sequency[3]])
	randomize()
	sequency[4] = int(rand_range(0,4))
	$HBoxContainer/s4.set_texture(sprites[sequency[4]])
	
	for child in get_parent().get_parent().get_children():
		if child.is_in_group("player"):
			players.insert(child.index, child)
			child.connect("button_pressed", self, "on_button_dmg")

func _process(delta):
	if position.distance_to(get_closer_player().position) < 100:
		walk1()
	else:
		velocity = Vector2(0,0)
		sprite.play("idle")

func summon(quantity):
	for i in range(-(quantity/2),(quantity/2)+1):
		var mnn = minion.instance()
		if sprite.flip_h:
			mnn.global_position = global_position + Vector2(-20,i*20)
		else:
			mnn.global_position = global_position + Vector2(20,i*20)
		get_parent().call_deferred("add_child", mnn)

func on_button_dmg(n, i):
	if index >= len(sequency) or not visivel.is_on_screen():
		return
	if n == sequency[min(4, predict_index)]:
		predict_index += 1
		if n == 0:
			players[i].use_magic(Color("#00ff00"), self)
		elif n == 1:
			players[i].use_magic(Color("#ff0000"), self)
		elif n == 2:
			players[i].use_magic(Color("#0000ff"), self)
		elif n == 3:
			players[i].use_magic(Color("#ffc0cb"), self)
	else:
		summon(1)

func walk1():
	var closer_player = get_closer_player()
	velocity = Vector2()
	if closer_player.position.x > position.x:
		velocity.x = -1
		sprite.play("walk")
		sprite.flip_h = false
	elif closer_player.position.x < position.x:
		velocity.x = 1
		sprite.play("walk")
		sprite.flip_h = true
	if closer_player.position.y > position.y:
		velocity.y = -1
		sprite.play("walk")
	elif closer_player.position.y < position.y:
		velocity.y = 1
		sprite.play("walk")
	velocity = velocity.normalized() * speed
	velocity = move_and_slide(velocity)


func _on_summon_time_timeout():
	summon(3)

func get_closer_player():
	var closer_player = players[0]
	for i in range(1,players.size()):
		if global_position.distance_to(players[i].global_position) < global_position.distance_to(closer_player.global_position):
			closer_player = players[i]
	
	return closer_player
