extends KinematicBody2D

export (int) var speed = 65

onready var sprite = $AnimatedSprite
onready var nav = get_parent().get_parent().get_node("Navigation2D")
onready var visivel = $VisibilityNotifier2D

var players = []

var velocity = Vector2()

var sequency = [0,0,0]
var path = PoolVector2Array()

var index = 0
var predict_index = 0

signal button_pressed

signal killed

func _ready():
	var sprites = [preload("res://assets/0x72_DungeonTilesetII_v1.3/frames/sym0_0.png"), preload("res://assets/0x72_DungeonTilesetII_v1.3/frames/sym1_0.png"), preload("res://assets/0x72_DungeonTilesetII_v1.3/frames/sym2_0.png"), preload("res://assets/0x72_DungeonTilesetII_v1.3/frames/sym3_0.png")]
	# gerando a sequencia de comandos a serem apertadas para derrotar o miniorc
	randomize()
	sequency[0] = int(rand_range(0,4))
	$HBoxContainer/s0.set_texture(sprites[sequency[0]])
	randomize()
	sequency[1] = int(rand_range(0,4))
	$HBoxContainer/s1.set_texture(sprites[sequency[1]])
	randomize()
	sequency[2] = int(rand_range(0,4))
	$HBoxContainer/s2.set_texture(sprites[sequency[2]])
	
	for child in get_parent().get_parent().get_children():
		if child.is_in_group("player"):
			players.insert(child.index, child)
			child.connect("button_pressed", self, "on_button_dmg")

func _physics_process(delta):
	move_along_path(speed * delta)

func on_button_dmg(n, i):
	if index >= len(sequency) or not visivel.is_on_screen():
		return
	if n == sequency[min(2,predict_index)]:
		predict_index += 1
		if n == 0:
			players[i].use_magic(Color("#00ff00"), self)
		elif n == 1:
			players[i].use_magic(Color("#ff0000"), self)
		elif n == 2:
			players[i].use_magic(Color("#0000ff"), self)
		elif n == 3:
			players[i].use_magic(Color("#ffc0cb"), self)
	else:
		speed = min(speed*1.2, 160)

func move_along_path(distance):
	var closer_player = get_closer_player()
	path = nav.get_simple_path(global_position, closer_player.global_position)
	
	if closer_player.position.x > position.x:
		sprite.flip_h = false
	elif closer_player.position.x < position.x:
		sprite.flip_h = true
		
	var start_point = position
	
	for i in range(path.size()):
		var distance_to_next = start_point.distance_to(path[0])
		if distance <= distance_to_next:
			position = start_point.linear_interpolate(path[0], distance/distance_to_next)
			sprite.play("walk")
			break
		elif path.size() == 1:
			position = path[0]
			sprite.play("idle")
			break
		distance -= distance_to_next
		start_point = path[0]
		path.remove(0)

func get_closer_player():
	var closer_player = players[0]
	for i in range(1,players.size()):
		if global_position.distance_to(players[i].global_position) < global_position.distance_to(closer_player.global_position):
			closer_player = players[i]
	
	return closer_player
