extends KinematicBody2D

export (int) var speed = 30

onready var sprite = $AnimatedSprite
onready var nav = get_parent().get_parent().get_node("Navigation2D")
onready var visivel = $VisibilityNotifier2D

var players = []

onready var spear = preload("res://scenes/lance.tscn")

var velocity = Vector2()
var path = PoolVector2Array()
var sequency = [0,0,0,0]

var index = 0
var predict_index = 0

signal killed

func _ready():
	var sprites = [preload("res://assets/0x72_DungeonTilesetII_v1.3/frames/sym0_0.png"), preload("res://assets/0x72_DungeonTilesetII_v1.3/frames/sym1_0.png"), preload("res://assets/0x72_DungeonTilesetII_v1.3/frames/sym2_0.png"), preload("res://assets/0x72_DungeonTilesetII_v1.3/frames/sym3_0.png")]
	# gerando a sequencia de comandos a serem apertadas para derrotar o miniorc
	randomize()
	sequency[0] = int(rand_range(0,4))
	$HBoxContainer/s0.set_texture(sprites[sequency[0]])
	randomize()
	sequency[1] = int(rand_range(0,4))
	$HBoxContainer/s1.set_texture(sprites[sequency[1]])
	randomize()
	sequency[2] = int(rand_range(0,4))
	$HBoxContainer/s2.set_texture(sprites[sequency[2]])
	sequency[3] = int(rand_range(0,4))
	$HBoxContainer/s3.set_texture(sprites[sequency[3]])
	
	for child in get_parent().get_parent().get_children():
		if child.is_in_group("player"):
			players.insert(child.index, child)
			child.connect("button_pressed", self, "on_button_dmg")

func _process(delta):
	if position.distance_to(get_closer_player().global_position) > 500:
		if $walk1_time.time_left == 0:
			$walk1_time.start()

func _physics_process(delta):
	if not $walk1_time.time_left == 0:
		move_along_path(speed * delta)
	elif not $walk2_time.time_left == 0:
		walk2()
		sprite.play("walk")
	else:
		sprite.play("idle")

func on_button_dmg(n, i):
	if index >= len(sequency) or not visivel.is_on_screen():
		return
	if n == sequency[min(3, predict_index)]:
		predict_index += 1
		if n == 0:
			players[i].use_magic(Color("#00ff00"), self)
		elif n == 1:
			players[i].use_magic(Color("#ff0000"), self)
		elif n == 2:
			players[i].use_magic(Color("#0000ff"), self)
		elif n == 3:
			players[i].use_magic(Color("#ffc0cb"), self)
	else:
		speed = min(speed*1.1, 160)
		shoot()

func walk2():
	var closer_player = get_closer_player()
	if closer_player.position.x > position.x && abs(closer_player.position.x-position.x) > abs(closer_player.position.y-position.y):
		velocity.x = 1
		sprite.flip_h = false
	elif closer_player.position.x < position.x && abs(closer_player.position.x-position.x) > abs(closer_player.position.y-position.y):
		velocity.x = -1
		sprite.flip_h = true
	elif closer_player.position.y > position.y && abs(closer_player.position.x-position.x) < abs(closer_player.position.y-position.y):
		velocity.y = 1
	elif closer_player.position.y < position.y && abs(closer_player.position.x-position.x) < abs(closer_player.position.y-position.y):
		velocity.y = -1
	velocity = velocity.normalized() * speed
	velocity = move_and_slide(velocity)
	

func shoot():
	var closer_player = get_closer_player()
	var spr = spear.instance()
	if closer_player.position.x < position.x:
		sprite.flip_h = true
	else:
		sprite.flip_h = false
	if sprite.flip_h:
		var pos = global_position-Vector2(20,0)
		spr.start(pos.angle_to_point(closer_player.global_position)+PI, pos)
	else:
		var pos = global_position+Vector2(20,0)
		spr.start(pos.angle_to_point(closer_player.global_position)+PI, pos)
	spr.connect("wall_collide", self, "_on_wall_collide")
	get_parent().get_parent().add_child(spr)



func _on_shoot_time_timeout():
	shoot()
	pass # Replace with function body.

func _on_wall_collide():
	$walk2_time.start()

func move_along_path(distance):
	var closer_player = get_closer_player()
	path = nav.get_simple_path(global_position, closer_player.global_position)
	
	if closer_player.position.x > position.x:
		sprite.flip_h = false
	elif closer_player.position.x < position.x:
		sprite.flip_h = true
	
	var start_point = position
	for i in range(path.size()):
		var distance_to_next = start_point.distance_to(path[0])
		if distance <= distance_to_next:
			position = start_point.linear_interpolate(path[0], distance/distance_to_next)
			sprite.play("walk")
			break
		elif path.size() == 1:
			position = path[0]
			sprite.play("idle")
			break
		distance -= distance_to_next
		start_point = path[0]
		path.remove(0)

func get_closer_player():
	var closer_player = players[0]
	for i in range(1,players.size()):
		if global_position.distance_to(players[i].global_position) < global_position.distance_to(closer_player.global_position):
			closer_player = players[i]
	
	return closer_player
