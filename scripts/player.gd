extends KinematicBody2D

export (int) var speed = 150
export (int) var index = 0

var hearts = 3

signal button_pressed

onready var sprite = $AnimatedSprite
onready var timer = $hitTimer
onready var hitbox = $Area2D
onready var weapon = $weapon
onready var magic = $magic
var velocity = Vector2()

var magic_node = preload("res://scenes/Magic.tscn")

func _ready():
	if index == 1:
		$AnimatedSprite.set_sprite_frames(load("res://assets/preSets/player1_sf.tres"))

func _unhandled_input(event):
	if $cooldown.is_stopped():
		var b1
		var b2
		var b3
		var b4
		if index == 0:
			b1 = event.is_action_pressed("b1_p1") && not event.is_echo()
			b2 = event.is_action_pressed("b2_p1") && not event.is_echo()
			b3 = event.is_action_pressed("b3_p1") && not event.is_echo()
			b4 = event.is_action_pressed("b4_p1") && not event.is_echo()
		elif index == 1:
			b1 = event.is_action_pressed("b1_p2") && not event.is_echo()
			b2 = event.is_action_pressed("b2_p2") && not event.is_echo()
			b3 = event.is_action_pressed("b3_p2") && not event.is_echo()
			b4 = event.is_action_pressed("b4_p2") && not event.is_echo()
		
		if b1:
			emit_signal("button_pressed", 0, index)
			magic.play("m0")
		elif b2:
			emit_signal("button_pressed", 1, index)
			magic.play("m1")
		elif b3:
			emit_signal("button_pressed", 2, index)
			magic.play("m2")
		elif b4:
			emit_signal("button_pressed", 3, index)
			magic.play("m3")
		else:
			return
		
		weapon.play("atk")
		magic.visible = true
		$cooldown.start()

func get_input():
	velocity = Vector2()
	
	var up
	var left
	var down
	var right
	if index == 0:
		up = Input.is_action_pressed('up_p1')
		right = Input.is_action_pressed('right_p1')
		down = Input.is_action_pressed('down_p1')
		left = Input.is_action_pressed('left_p1')
	elif index == 1:
		up = Input.is_action_pressed('up_p2')
		right = Input.is_action_pressed('right_p2')
		down = Input.is_action_pressed('down_p2')
		left = Input.is_action_pressed('left_p2')
	
	if right:
		velocity.x += 1
		sprite.flip_h = false
		weapon.flip_h = false
		weapon.position.x = 13.723
	if left:
		velocity.x -= 1
		sprite.flip_h = true
		weapon.flip_h = true
		weapon.position.x = -13.947
	if down:
		velocity.y += 1
	if up:
		velocity.y -= 1
		
	if velocity != Vector2(0,0):
		sprite.play("walk")
	else:
		sprite.play("idle")
	
	velocity = velocity.normalized() * speed

func _physics_process(delta):
	get_input()
	velocity = move_and_slide(velocity)
	
#func _process(delta):
	#weapon.play("idle")

func _on_Area2D_body_entered(body):
	if body.is_in_group("enemy") or body.is_in_group("bullet"):
		print("ai")
		body.queue_free()
		hearts -=1
		timer.start()
		if hearts <= 0:
			get_tree().quit()
			

func use_magic(color, target):
	if pressing_a_lot():
		print("fool!")
		hearts -= 1
		timer.start()
		if hearts <= 0:
			get_tree().quit()
		return

	var magic_instance = magic_node.instance()
	magic_instance.start(color, target)
	if sprite.flip_h:
		magic_instance.position = position + Vector2(-6,-16)
	else:
		magic_instance.position = position + Vector2(6,-16)
	get_parent().add_child(magic_instance) 

func pressing_a_lot():
	var pressing = 0
	
	if Input.is_action_pressed("b1"):
		pressing += 1
	if Input.is_action_pressed("b2"):
		pressing += 1
	if Input.is_action_pressed("b3"):
		pressing += 1
	if Input.is_action_pressed("b4"):
		pressing += 1
	
	return pressing > 1

func _on_weapon_animation_finished():
	weapon.play("idle")
	pass # Replace with function body.


func _on_magic_animation_finished_magic():
	magic.visible = false
	pass # Replace with function body.
