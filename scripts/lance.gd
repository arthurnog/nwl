extends KinematicBody2D

var vel = Vector2()
var speed = 160
var bounce = 0

signal wall_collide

func start(dir, pos):
	global_position = pos
	rotation = dir
	vel = Vector2(speed,0).rotated(rotation)
	
func _physics_process(delta):
	var collision = move_and_collide(vel*delta)
	if collision:
		if not collision.collider.is_in_group("player"):
			emit_signal("wall_collide")
		vel = vel.bounce(collision.normal)
		rotation = vel.angle()
		bounce += 1
		if bounce > 3:
			queue_free()
