extends Area2D

var target
export (float) var speed = 1200
var velocity

func _process(delta):
	var wr = weakref(target)
	if not wr.get_ref() or (target.position - position).length() <= 5:
		set_process(false)
		$Particles2D.set_emitting(false)
		$Timer.start()
		return
	
	velocity = (target.position - position).normalized() * speed
	position += velocity * delta

func start(color, target):
	self.target = target
	set_modulate(color)

func _on_Timer_timeout():
	queue_free()


func _on_Magic_body_entered(body):
	if not body.is_in_group("enemy"): return
	
	if body == target:
		if body.index < body.sequency.size():
			if (body.sequency[body.index] == 0 and get_modulate() != Color("#00ff00")) or (body.sequency[body.index] == 1 and get_modulate() != Color("#ff0000")) or (body.sequency[body.index] == 2 and get_modulate() != Color("#0000ff")) or (body.sequency[body.index] == 3 and get_modulate() != Color("#ffc0cb")):
				return
			
			body.index += 1
			if body.get_node("HBoxContainer").get_children().size() > 0:
				body.get_node("HBoxContainer").get_child(0).queue_free()
			
			if body.get_node("HBoxContainer").get_children().size() < 2:
				if body.is_in_group("necromancer"):
					body.summon(3)
				body.queue_free()
				body.emit_signal("killed")
