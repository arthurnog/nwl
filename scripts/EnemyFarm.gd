extends Node

var enemy0 = preload("res://scenes/miniOrc.tscn")
var enemy1 = preload("res://scenes/enemy2.tscn")
var enemy2 = preload("res://scenes/enemy3.tscn")
var enemy3 = preload("res://scenes/necromancer.tscn")
onready var timer = $Timer

var kills = 0

var players = []

func _ready():
	for child in get_parent().get_children():
		if child.is_in_group("player"):
			players.insert(child.index, child)
	

func spawn():
	var spawnPosition
	var close_to_players = true
	while close_to_players:
		randomize()
		spawnPosition = Vector2(rand_range(32,480),rand_range(32,304))
		for p in players:
			if spawnPosition.distance_to(p.global_position) > 50:
				close_to_players = false
			else:
				close_to_players = true
				break

	var enemyInstance
	if kills <= 10:
		enemyInstance = enemy0.instance()
	elif kills <=20:
		randomize()
		var flag = int(rand_range(0,2))
		if flag == 0:
			enemyInstance = enemy0.instance()
		else:
			enemyInstance = enemy1.instance()
	elif kills <= 30:
		randomize()
		var flag = int(rand_range(0,4))
		if flag == 0:
			enemyInstance = enemy1.instance()
		elif flag == 1:
			enemyInstance = enemy2.instance()
		else:
			enemyInstance = enemy0.instance()
	elif kills > 30:
		randomize()
		var flag = int(rand_range(0,5))
		if flag == 0:
			enemyInstance = enemy1.instance()
		elif flag == 1:
			enemyInstance = enemy2.instance()
		elif flag == 2:
			enemyInstance = enemy3.instance()
		else:
			enemyInstance = enemy0.instance()
	
	enemyInstance.global_position = spawnPosition
	enemyInstance.connect("killed", self, "kill_counting")
	
	if enemyInstance.get_node("Area2D").get_overlapping_areas().size()>0:
		spawn()
		return
	if enemyInstance.is_in_group("necromancer"):
		for child in get_children():
			if child.is_in_group("necromancer"):
				spawn()
				return
				
	add_child(enemyInstance)

func kill_counting():
	kills += 1
	get_parent().get_node("GUI/Container/killCounter").text = "Eliminados = " + str(kills)


func _on_Timer_timeout():
	randomize()
	timer.set_wait_time(rand_range(1,5))
	spawn()
	pass # Replace with function body.
