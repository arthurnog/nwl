extends Camera2D

var players = []

onready var tween = $Tween

func _ready():
	for child in get_parent().get_children():
		if child.is_in_group("player"):
			players.append(child)

func _process(delta):
	var mean_pos = players[0].global_position
	for i in range(1,players.size()):
		mean_pos = (mean_pos+players[i].global_position)/2
	global_position = mean_pos
	
	if players.size() > 1:
		var max_x = players[0].global_position.x
		var min_x = players[0].global_position.x
		var max_y = players[0].global_position.y
		var min_y = players[0].global_position.y
		for i in range(1,players.size()):
			if players[i].global_position.x > max_x:
				max_x = players[i].global_position.x
			if players[i].global_position.x < min_x:
				min_x = players[i].global_position.x
			if players[i].global_position.y > max_y:
				max_y = players[i].global_position.y
			if players[i].global_position.y < min_y:
				min_y = players[i].global_position.y
		
		var distance = max(max_x-min_x, max_y-min_y)
		
		if distance < 200:
			tween.interpolate_property(self, "zoom", zoom, Vector2(0.5,0.5), 0, Tween.TRANS_LINEAR, Tween.EASE_IN)
		elif distance > 300:
			tween.interpolate_property(self, "zoom", zoom, Vector2(0.75, 0.75), 0, Tween.TRANS_LINEAR, Tween.EASE_IN)
		else:
			tween.interpolate_property(self, "zoom", zoom, Vector2(distance/400, distance/400), 0, Tween.TRANS_LINEAR, Tween.EASE_IN)
		
		tween.start()
	
